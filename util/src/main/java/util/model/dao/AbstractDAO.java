package util.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AbstractDAO<T, ID> extends JpaRepository<T, ID> {

}

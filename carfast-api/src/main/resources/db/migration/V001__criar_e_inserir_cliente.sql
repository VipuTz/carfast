CREATE TABLE acessorio (
    id BIGINT NOT NULL AUTO_INCREMENT,
    descricao VARCHAR(255),
    PRIMARY KEY (id)
)  ENGINE=INNODB;
CREATE TABLE carro_acessorio (
    id_carro BIGINT NOT NULL,
    id_acessorio BIGINT NOT NULL
)  ENGINE=INNODB;
CREATE TABLE cliente (
    id BIGINT NOT NULL AUTO_INCREMENT,
    cnpj BIGINT,
    cpf BIGINT,
    dt_nascimento DATE,
    email VARCHAR(255),
    nome VARCHAR(15) NOT NULL,
    rg BIGINT,
    sobrenome VARCHAR(255),
    ativo TINYINT NULL,
    PRIMARY KEY (id)
)  ENGINE=INNODB;
CREATE TABLE cliente_enderecos (
    id_cliente BIGINT NOT NULL,
    bairro VARCHAR(255),
    cep BIGINT,
    cidade VARCHAR(255),
    complemento VARCHAR(255),
    logradouro VARCHAR(255),
    uf VARCHAR(255)
)  ENGINE=INNODB;
CREATE TABLE cliente_telefones (
    id_cliente BIGINT NOT NULL,
    number BIGINT,
    tipo VARCHAR(255)
)  ENGINE=INNODB;
CREATE TABLE fabricante_carro (
    id BIGINT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(255),
    PRIMARY KEY (id)
)  ENGINE=INNODB;
CREATE TABLE modelo_carro (
    id BIGINT NOT NULL AUTO_INCREMENT,
    descricao VARCHAR(255),
    id_fabricante BIGINT,
    PRIMARY KEY (id)
)  ENGINE=INNODB;
CREATE TABLE veiculo (
    id BIGINT NOT NULL AUTO_INCREMENT,
    data_ano DATE,
    chassi BIGINT,
    combustivel VARCHAR(255),
    cor VARCHAR(255),
    data_expedicao DATE,
    km FLOAT,
    local VARCHAR(255),
    data_modelo DATE,
    motor VARCHAR(255),
    numero_motor VARCHAR(255),
    placa VARCHAR(255),
    renavan BIGINT,
    valor_compra DECIMAL(19 , 2 ),
    valor_venda DECIMAL(19 , 2 ),
    id_modelo BIGINT,
    PRIMARY KEY (id)
)  ENGINE=INNODB;
ALTER TABLE cliente ADD CONSTRAINT UK_2vf89ija5fea0souakqh3bg59 UNIQUE (cnpj);
ALTER TABLE cliente ADD CONSTRAINT UK_r1u8010d60num5vc8fp0q1j2a UNIQUE (cpf);
ALTER TABLE cliente ADD CONSTRAINT UK_a0reg3hoo4hfq3o8mkoascfsm UNIQUE (rg);
ALTER TABLE carro_acessorio ADD CONSTRAINT FK5jyer1bs4edtrncs4s12gjucs FOREIGN KEY (id_acessorio) REFERENCES acessorio (id);
ALTER TABLE carro_acessorio ADD CONSTRAINT FKn8xogxkyehf0ew0neo1qpuh8u FOREIGN KEY (id_carro) REFERENCES veiculo (id);
ALTER TABLE cliente_enderecos ADD CONSTRAINT FKsl12jy3ohdg01o196vs6tgqqv FOREIGN KEY (id_cliente) REFERENCES cliente (id);
ALTER TABLE cliente_telefones ADD CONSTRAINT FK6xahvkpwwdm7nbtjxm8cwoeif FOREIGN KEY (id_cliente) REFERENCES cliente (id);
ALTER TABLE modelo_carro ADD CONSTRAINT FK5y24n1dj4f7kljgwv8gst6n42 FOREIGN KEY (id_fabricante) REFERENCES fabricante_carro (id);
ALTER TABLE veiculo ADD CONSTRAINT FKosrtx8niy7451lqlm4eftcovw FOREIGN KEY (id_modelo) REFERENCES modelo_carro (id);
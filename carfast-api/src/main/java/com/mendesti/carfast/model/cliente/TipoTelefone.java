package com.mendesti.carfast.model.cliente;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum TipoTelefone {

	COMERCIAL(1, "COMERCIAL"), 
	RESISDENCIAL(2, "RESISDENCIAL"), 
	CELULAR(3, "CELULAR");

	private int codigo;
	private String descricao;

	private TipoTelefone(int codigo, String descricao) {
		this.setCodigo(codigo);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static TipoTelefone getTipo(int codigo) {
		for(TipoTelefone e: TipoTelefone.values()) {
			if(e.getCodigo() == codigo) {
				return e;
			}
		}
		return null;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}

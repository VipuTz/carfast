package com.mendesti.carfast.model.veiculo;

public enum TipoCombustivel {

	GASOLINA,
	ETANOL,
	GASOLINA_GNV,
	ETANOL_GNV,
	GNV,
	DIESEL,
	TOTALFLEX
}

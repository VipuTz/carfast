package com.mendesti.carfast.model.veiculo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import util.model.AbstractEntity;

@Entity
@Table(name = "modelo_carro")
public class ModeloCarro extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "descricao")
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "id_fabricante", referencedColumnName = "id")
	private Fabricante fabricante;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

}

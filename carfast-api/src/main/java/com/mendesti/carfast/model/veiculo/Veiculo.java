package com.mendesti.carfast.model.veiculo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import util.model.AbstractEntity;

@Entity
@Table(name = "veiculo")
public class Veiculo extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "placa")
	private String placa;

	@Column(name = "cor")
	private String cor;

	@Column(name = "renavan")
	private Long renavan;

	@Column(name = "chassi")
	private Long chassi;
	
	@Column(name = "data_ano")
	private LocalDate ano;
	
	@Column(name = "data_modelo")
	private LocalDate modelo;
	
	@Column(name = "local")
	private String local;
	
	@Column(name = "data_expedicao")
	private LocalDate dataExpedicao;
	
	@Column(name = "numero_motor")
	private String numeroMotor;
	
	@Column(name = "motor")
	private String motor;
	
	@Column(name = "valor_compra")
	private BigDecimal valorCompra;
	
	@Column(name = "valor_venda")
	private BigDecimal valorVenda;

	@Column(name = "km")
	private Float km;

	@ManyToOne
	@JoinColumn(name = "id_modelo")
	private ModeloCarro modeloCarro;

	@Enumerated(EnumType.STRING)
	@Column(name = "combustivel")
	private TipoCombustivel combustivel;

	@ManyToMany
	@JoinTable(name = "carro_acessorio", joinColumns = @JoinColumn(name = "id_carro"), inverseJoinColumns = @JoinColumn(name = "id_acessorio"))
	private List<Acessorio> acessorios;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Long getRenavan() {
		return renavan;
	}

	public void setRenavan(Long renavan) {
		this.renavan = renavan;
	}

	public Long getChassi() {
		return chassi;
	}

	public void setChassi(Long chassi) {
		this.chassi = chassi;
	}

	public LocalDate getAno() {
		return ano;
	}

	public void setAno(LocalDate ano) {
		this.ano = ano;
	}

	public LocalDate getModelo() {
		return modelo;
	}

	public void setModelo(LocalDate modelo) {
		this.modelo = modelo;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public LocalDate getDataExpedicao() {
		return dataExpedicao;
	}

	public void setDataExpedicao(LocalDate dataExpedicao) {
		this.dataExpedicao = dataExpedicao;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public BigDecimal getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(BigDecimal valorCompra) {
		this.valorCompra = valorCompra;
	}

	public BigDecimal getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(BigDecimal valorVenda) {
		this.valorVenda = valorVenda;
	}

	public Float getKm() {
		return km;
	}

	public void setKm(Float km) {
		this.km = km;
	}

	public ModeloCarro getModeloCarro() {
		return modeloCarro;
	}

	public void setModeloCarro(ModeloCarro modeloCarro) {
		this.modeloCarro = modeloCarro;
	}

	public TipoCombustivel getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(TipoCombustivel combustivel) {
		this.combustivel = combustivel;
	}

	public List<Acessorio> getAcessorios() {
		return acessorios;
	}

	public void setAcessorios(List<Acessorio> acessorios) {
		this.acessorios = acessorios;
	}

}

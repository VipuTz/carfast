package com.mendesti.carfast.model.veiculo;

import javax.persistence.Entity;
import javax.persistence.Table;

import util.model.AbstractEntity;

@Entity
@Table(name = "fabricante_carro")
public class Fabricante  extends AbstractEntity{

	private static final long serialVersionUID = 1L;
	
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}

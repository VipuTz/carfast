package com.mendesti.carfast.model.cliente;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import util.model.AbstractEntity;

@Entity
@Table(name = "cliente")
public class Cliente extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 3, max = 15)
	@Column(name = "nome", nullable = false, length = 15)
	private String nome;

	@Column(name = "sobrenome")
	private String sobreNome;

	@Column(name = "cpf", unique = true)
	private Long cpf;

	@Column(name = "cnpj", unique = true)
	private Long cnpj;

	@Column(name = "rg", unique = true)
	private Long rg;

	@Column(name = "email")
	private String email;
	
	@Column(name = "dt_nascimento")
	private LocalDate dataNascimento;

	@ElementCollection
	@CollectionTable(joinColumns = { @JoinColumn(name = "id_cliente", referencedColumnName = "id") })
	private List<Endereco> enderecos;

	@ElementCollection
	@CollectionTable(joinColumns = { @JoinColumn(name = "id_cliente", referencedColumnName = "id") })
	private List<Telefone> telefones;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Long getCnpj() {
		return cnpj;
	}

	public void setCnpj(Long cnpj) {
		this.cnpj = cnpj;
	}

	public Long getRg() {
		return rg;
	}

	public void setRg(Long rg) {
		this.rg = rg;
	}

	public String getSobreNome() {
		return sobreNome;
	}

	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}

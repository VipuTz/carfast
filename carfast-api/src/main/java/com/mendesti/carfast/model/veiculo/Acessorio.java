package com.mendesti.carfast.model.veiculo;

import javax.persistence.Entity;
import javax.persistence.Table;

import util.model.AbstractEntity;

@Entity
@Table(name = "acessorio")
public class Acessorio extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

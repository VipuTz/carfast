package com.mendesti.carfast.repository;

import com.mendesti.carfast.model.cliente.Cliente;

import util.model.dao.AbstractDAO;

public interface ClienteRepository extends AbstractDAO<Cliente, Long> {
	

}

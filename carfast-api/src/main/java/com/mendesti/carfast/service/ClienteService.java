package com.mendesti.carfast.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mendesti.carfast.model.cliente.Cliente;
import com.mendesti.carfast.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public List<Cliente> listarClientes() {
		return clienteRepository.findAll();
	}

	public Cliente criar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	public Cliente buscarPeloId(Long id) {

		Optional<Cliente> cliente = clienteRepository.findById(id);
		if (!cliente.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return cliente.get();
	}

	public void remover(Long id) {
		clienteRepository.deleteById(id);
	}

	public Cliente atualizar(Long id, Cliente cliente) {

		if (!clienteRepository.existsById(id)) {
			throw new EmptyResultDataAccessException(1);
		}
		Cliente clienteSalva = clienteRepository.findById(id).get();
		BeanUtils.copyProperties(cliente, clienteSalva, "id");
		return clienteRepository.save(clienteSalva);

	}
}
